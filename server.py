#!/usr/bin/python3
# -*- coding: utf-8 -*-


import socketserver
import sys
import simplertp
import secrets


class EchoHandler(socketserver.DatagramRequestHandler):

    def handle(self):
        #leemos mensaje cliente
        mensaje = self.rfile.read().decode("utf-8")
        metodo = mensaje.split()[0]
        print(metodo, "recibido")
        #distinguimos los distintos valores del cliente
        if metodo == "INVITE":
            try:
                line = mensaje.split("\r\n")[0]
                NAME = line.split()[1].split(":")[1].split("@")[0]
                IP = line.split()[1].split(":")[1].split("@")[1]
                SDP = "Content-Type: application/SDP\r\n\r\nv=0\r\no=" + NAME + " " + IP + "\r\ns=alvaro sesion\r\nt=0\r\nm=audio 34543 RTP\r\n\r\n"
                m = "SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing\r\n\r\nSIP/2.0 200 OK\r\n\r\n"
                self.wfile.write(bytes(m+SDP, "utf-8") + b"\r\n")
                BIT = secrets.randbelow(1)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(version=2, marker=BIT, payload_type=14, ssrc=200002)
                audio = simplertp.RtpPayloadMp3(fichero_audio)
                simplertp.send_rtp_packet(RTP_header, audio, "127.0.0.1", 23032)
            except IndexError:
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")
        elif metodo == "BYE":
            self.wfile.write(b"SIP/2.0 200 OK\r\n")
        elif metodo == "ACK":
            BIT = secrets.randbelow(1)
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(version=2, marker=BIT, payload_type=14, ssrc=200002)
            audio = simplertp.RtpPayloadMp3(fichero_audio)
            simplertp.send_rtp_packet(RTP_header, audio, "127.0.0.1", 23032)

        else:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n")


if __name__ == "__main__":
    try:
        if len(sys.argv) != 4:
            sys.exit("Usage: python3 server.py IP PORT fichero_audio")
        IP = sys.argv[1]
        PORT = int(sys.argv[2])
        fichero_audio = sys.argv[3]

    except ValueError:
        sys.exit("Usage: python3 server.py IP PORT fichero_audio")

    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer((IP, PORT), EchoHandler)
    print("Listening...")
    serv.serve_forever()()
