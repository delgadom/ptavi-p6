#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socket
import sys


#Introducimos los parametros del cliente
if len(sys.argv) == 3:
    METH = str(sys.argv[1])
    NAME = sys.argv[2].split("@")[0]
    IP = sys.argv[2].split("@")[1].split(":")[0]
    PORT = int(sys.argv[2].split("@")[1].split(":")[1])
else:
    sys.exit("Usage: python3 client.py METODO UA@IPUA:PORTsip")

#Definimos que hace cada metodo

if METH == 'BYE':
    info = "BYE sip:" + NAME + "@" + IP+ " SIP/2.0\r\n"
    ACK = "0"

elif METH == 'INVITE':
    info = "INVITE sip:" + NAME + "@" + IP + " SIP/2.0\r\n"
    ACK = "ACK sip:" + NAME + "@" + IP + " SIP/2.0\r\n"

else:
    info = METH + " sip:" + NAME + "@" + IP + " SIP/2.0\r\n"

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP, PORT))

    print("Enviando: " + info)
    my_socket.send(bytes(info, "utf-8") + b"\r\n")
    data = my_socket.recv(1024).decode("utf-8")
    my_socket.send(bytes(ACK, "utf-8") + b"\r\n")
    print('Recibido')
    print("")
    print(data)
    print("Terminando socket...")
print("Fin.")
